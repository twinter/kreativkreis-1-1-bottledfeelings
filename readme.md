# Bottled Feelings

A project by Tom Winter during Phase 1 of the first edition of the
"KreativKreis" project.

The emotions are based on the model described by Parrot in 2001 but only
includes the six primary emotions.

The project lives here:
<https://gitlab.com/twinter/kreativkreis-1-1-bottledfeelings>.

## controls

Pause the loop with ESC or space, unpause with space.

## running it

### Option 1: with npm directly on host

1. install npm
2. run `npm install`
3. run `npm run start`
4. the project will be available on <localhost:3000>

### Option 2: with docker-compose

1. run `docker-compose -f docker-compose-without-reverse-proxy.yml up`
2. the project will be available on <localhost:3000>

### Option 3: semi-production with [reverse proxy](https://gitlab.com/twinter/docker-compose-reverse-proxy)

1. set the environment variables in docker-compose.yml to fit your setup
2. run `docker-compose up` or use a [systemd service](https://gitlab.com/twinter/docker-compose-systemd-service)
3. the project will be available on the domain as configured in docker-compose.yml

