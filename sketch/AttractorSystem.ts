/**
 * This is a factory and manager for attractor systems at the same time.
 */
class AttractorSystem {
  // class attributes (i.e. attributes of the factory)
  // a list of all systems
  private static attractor_systems: { [name: string]: AttractorSystem } = {}

  // instance attributes (i.e. attributes of one specific system)
  private name: string
  private objects: Attractor[] = []
  private objects_unaffected: Attractor[] = []
  // if the effect of the system is inverted (false: force decreases with distance, true: force increases with distance, disables max_distance)
  inverted: boolean = false
  // reduce the calculated distance by a an offset, creates areas of least/most force in a circle with this radius
  offset: number = 0
  // strength of the forces in this system (i.e. the gravitational constant used in the calculations
  strength: number
  // distance after which we stop calculating any forces to reduce required processing power
  max_distance: number = max(width, height) * 0.3

  // private constructor to prevent instantiation from outside
  private constructor(name: string, strength: number, inverted: boolean) {
    this.name = name
    this.strength = strength
    this.inverted = inverted
  }

  /**
   * create a new attractor system of a specific name, the name has to be unique
   */
  static get_new_attractor_system(name: string, strength: number, inverted: boolean = false): AttractorSystem {
    // check if the name already exists
    if (this.attractor_systems.hasOwnProperty(name))
      throw new Error(`there is already an attractor with the name "${name}" registered, can't register another one`)

    // create new system, register it and return it
    const new_system = new AttractorSystem(name, strength, inverted)
    this.attractor_systems[name] = new_system
    return new_system
  }

  add_attractor(a: Attractor) {
    this.objects.push(a)
  }

  /**
   * add an attractor to the system that affects other (regular) objects but is not affected by others in this system
   */
  add_unaffected_attractor(a: Attractor) {
    this.objects_unaffected.push(a)
  }

  /**
   * Calculate the attraction (basically gravitational) force vector on the first object by the second one.
   * All masses are assumed to be 1.
   *
   * It uses the formula from https://physics.stackexchange.com/a/17291
   */
  private calculate_force_on_object_by_object(o_target: Attractor, o_cause: Attractor): p5.Vector {
    // vector describing the distance
    let r = Vector.sub(o_cause.position, o_target.position)
    // scalar describing the distance
    let distance = r.mag()

    if (this.offset > 0) {
      distance -= this.offset
      r.setMag(distance)
    }

    // return no force if objects are too far away
    if (this.max_distance > 0 && distance > this.max_distance && !this.inverted)
      return createVector(0, 0)

    if (this.inverted) {
      // calculate the force with f = strength * r * distance, strength is reduced so we don't have to use tiny numbers
      let f: p5.Vector = Vector.mult(r, this.strength / 1000)
      f.mult(distance)
      return f
    } else {
      // calculate the force with f = (strength * r)/distance^3
      let f: p5.Vector = Vector.mult(r, this.strength)
      f.div(Math.pow(distance, 3))
      return f
    }
  }

  /**
   * destroy this attractor system, no more updates will happen and the name can be assigned again
   */
  destroy() {
    delete AttractorSystem.attractor_systems[this.name]
  }

  /**
   * update all attractor systems
   */
  static update() {
    for (const name in this.attractor_systems)
      this.attractor_systems[name]._update()
  }

  /**
   * calculate and apply forces from the system, don't update the objects here to avoid double updates in case it is in more than one system
   */
  private _update() {
    // calculate and apply forces from unaffected objects on regular objects
    for (const o of this.objects_unaffected) {
      for (const o2 of this.objects)
        o2.apply_force(this.calculate_force_on_object_by_object(o2, o))
    }

    // calculate forces between regular objects
    for (let i = 0; i < this.objects.length; i++) {
      for (let j = i + 1; j < this.objects.length; j++) {
        const o1 = this.objects[i]
        const o2 = this.objects[j]
        const force = this.calculate_force_on_object_by_object(o1, o2)
        o1.apply_force(force)
        o2.apply_force(force.mult(-1))
      }
    }
  }

  apply_one_time_force(position: p5.Vector, strength: number = this.strength) {
    // save original strength so we can reset it when we're done and replace it with the strength given as argument
    const strength_orig = this.strength
    this.strength = strength

    // create a (temporary) fake attractor
    let force_origin = new Attractor(position.x, position.y)

    // calculate and apply forces to objects
    for (const o of this.objects) {
      const force = this.calculate_force_on_object_by_object(o,  force_origin)
      o.apply_force(force)
    }

    // restore original strength
    this.strength = strength_orig
  }
}
