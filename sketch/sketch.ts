///<reference path="AttractorSystem.ts"/>
let global_attractor: AttractorSystem
let bottle: Bottle
let objects: Attractor[]
let paused: boolean = false
let mouse_attractor: Attractor;

function setup() {
  setupScreen()

  bottle = new Bottle()

  // create global attractor system
  global_attractor = AttractorSystem.get_new_attractor_system('global', -100)
  global_attractor.max_distance = -1

  // create mouse attractor and add it to the system
  mouse_attractor = new Attractor(mouseX, mouseY)
  global_attractor.add_unaffected_attractor(mouse_attractor)

  // add emotions
  objects = []
  for (const e of [new Anger(), new Fear(), new Joy(), new Love(), new Sadness(), new Surprise()]) {
    objects.push(e)
    global_attractor.add_attractor(e)
  }
}

function draw() {
  // update stuff
  bottle.update()
  mouse_attractor.position.x = mouseX
  mouse_attractor.position.y = mouseY
  AttractorSystem.update()
  for (const o of objects)
    o.update()

  // draw stuff
  background(201)
  bottle.draw()
  CollisionDetector.draw()  // used to draw boundaries if enabled
  for (const o of objects)
    o.draw()
}

/**
 * p5 builtin function to handle window resize events
 */
function windowResized() {
  setupScreen()
}

function setupScreen() {
  createCanvas(windowWidth, windowHeight)
  CollisionDetector.registerWindowBoundaries()
}

function pause() {
  paused = true
  noLoop()
  console.log('loop paused')
}

function unpause() {
  paused = false
  loop()
  console.log('resuming loop')
}

function toggle_pause() {
  if (paused)
    unpause()
  else
    pause()
}

/**
 * p5 builtin function to handle key press events
 */
function keyPressed() {
  if (keyCode === ESCAPE)
    pause()
  else if (key === ' ')
    toggle_pause()
}

function mouseClicked() {
  if (!paused)
    global_attractor.apply_one_time_force(createVector(mouseX, mouseY), -10000)
}
