class BottleLine {
  dot_distance: number = 20
  dot_displacement_max: number = 15
  lines_amount: number = 5
  private p1: p5.Vector
  private p2: p5.Vector

  constructor(p1: p5.Vector, p2: p5.Vector) {
    this.p1 = p1
    this.p2 = p2

    CollisionDetector.registerBoundaryLine(p1.x, p1.y, p2.x, p2.y)
  }

  update() {
  }

  draw() {
    push()
    stroke(0)
    strokeWeight(3)
    noFill()
    // spread dots evenly along the line with this.dot_distance as target, calculate real distance between dots based on that
    const distance = Vector.sub(this.p1, this.p2).mag()
    const dots_to_place = round(distance / this.dot_distance)
    const dot_distance_adapted = distance / dots_to_place

    // get a unit vector pointing in the direction of the line so we can easily place the dots using vectors
    let line_direction = Vector.sub(this.p2, this.p1).normalize()

    for (let i = 0; i < this.lines_amount; i++) {
      beginShape()
      // place dots and displace them randomly, we place dots_to_place + 1 dots to get one on both ends
      for (let j = 0; j <= dots_to_place; j++) {
        let dot_pos = this.p1.copy()
        dot_pos.add(Vector.mult(line_direction, j * dot_distance_adapted))
        let offset = createVector()
        offset.x =  noise((dot_pos.x + i) * 0.001, (dot_pos.y + j) * 0.001, frameCount * 0.01) * 10
        offset.y =  noise((dot_pos.x + i) * 0.001, (dot_pos.y + j) * 0.001, 100 + frameCount * 0.01) * 10
        offset.normalize()
        offset.mult(noise(dot_pos.x + i, dot_pos.y + j,frameCount * 0.01) * this.dot_displacement_max)
        dot_pos.add(offset)
        vertex(dot_pos.x, dot_pos.y)
      }
      endShape()
    }
    pop()
  }
}
