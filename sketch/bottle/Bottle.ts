class Bottle {
  lines: BottleLine[] = []

  constructor() {
    // define vectors of the relevant points
    const body_bottom_left = createVector(width / 4, height - 25)
    const body_bottom_right = createVector((3 / 4) * width, height - 25)
    const body_top_left = createVector(width / 4, height / 2)
    const body_top_right = createVector((3 / 4) * width, height / 2)
    const opening_bottom_left = createVector((3 / 8) * width, (3 / 8) * height)
    const opening_bottom_right = createVector((5 / 8) * width, (3 / 8) * height)
    const opening_top_left = createVector((3 / 8) * width, (2 / 8) * height)
    const opening_top_right = createVector((5 / 8) * width, (2 / 8) * height)

    // create lines
    this.lines.push(new BottleLine(opening_top_left, opening_bottom_left))
    this.lines.push(new BottleLine(opening_bottom_left, body_top_left))
    this.lines.push(new BottleLine(body_top_left, body_bottom_left))
    this.lines.push(new BottleLine(body_bottom_left, body_bottom_right))
    this.lines.push(new BottleLine(body_bottom_right, body_top_right))
    this.lines.push(new BottleLine(body_top_right, opening_bottom_right))
    this.lines.push(new BottleLine(opening_bottom_right, opening_top_right))
  }

  update() {
    for (const l of this.lines)
      l.update()
  }

  draw() {
    for (const l of this.lines)
      l.draw()
  }
}
