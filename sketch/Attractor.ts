const Vector = p5.Vector

class Attractor {
  position: p5.Vector
  draw_enabled: boolean = false
  private speed: p5.Vector
  public speed_max: number = 3
  public speed_min: number = 0

  constructor(pos_x?: number, pos_y?: number) {
    this.position = createVector()
    this.position.x = pos_x ?? random(0, width)
    this.position.y = pos_y ?? random(0, height)

    // generate a unit vector in a random direction multiply to change the speed
    this.speed = Vector.random2D()
    this.speed.mult(random(1, 2))
  }

  apply_force(force: p5.Vector) {
    this.speed.add(force)
    this.speed.limit(this.speed_max)

    if (this.speed_min > 0) {
      const speed_abs = this.speed.mag()
      if (speed_abs < this.speed_min)
        this.speed.setMag(this.speed_min)
    }
  }

  /**
   * Moves this point a tiny bit to the target if the distance squared (!) is bigger than some limit.
   * We are comparing against the square of the distance because it is a lot faster and we will call this function very often.
   */
  nudge_to_point_after_dist_sq(target: Attractor, dist_sq: number) {
    const distance = Vector.sub(target.position, this.position)
    if (distance.magSq() > dist_sq)
      this.position.add(distance.mult(0.01))
  }


  update() {
    // calculate new position
    let position_new = Vector.add(this.position, this.speed)

    // check if traveled line intersects with a boundary, get reflected vector if so
    const reflectedVector = CollisionDetector.getReflectedVector({p1: this.position, p2: position_new})
    if (reflectedVector !== null) {
      this.speed = reflectedVector
      position_new = Vector.add(this.position, this.speed)
    }

    // apply changed position
    this.position = position_new

    // check if new position is outside of the window, respawn somewhere random if it is
    if (this.position.x < 0 || this.position.y < 0 || this.position.x > width || this.position.y > height) {
      this.position.x = random(0, width)
      this.position.y = random(0, height)
    }
  }

  draw() {
    if (this.draw_enabled) {
      push()
      strokeWeight(5)
      point(this.position.x, this.position.y)
      pop()
    }
  }
}
