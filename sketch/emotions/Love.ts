class Love extends Attractor {
  static satellites_amount: number = 10
  attractor_system_satellites: AttractorSystem;
  attractor_systems_with_middle: AttractorSystem[] = []
  satellites: Attractor[] = []
  repelling_end_frame: number = 0  // number of frame when repelling ends
  repelling_time_min: number = 30
  repelling_time_max: number = 180
  repelling_chance: number
  repelling_chance_base: number = 0.1  // chance to start repelling others in %
  repelling_chance_increase_per_frame: number = 0.1

  constructor() {
    super()

    // add attractor object that repels satellites from each other
    this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('love_satellites', -35)

    for (let i = 0; i < Love.satellites_amount; i++) {
      // create new attractor
      const offset = Vector.random2D().mult(15)
      const new_position = Vector.add(this.position, offset)
      const new_attractor = new Attractor(new_position.x, new_position.y)
      this.satellites.push(new_attractor)

      // add to system that repels satellites from each other
      this.attractor_system_satellites.add_attractor(new_attractor)

      // create attractor with middle
      let new_attractor_system = AttractorSystem.get_new_attractor_system(`love_satellite_${i}`, 15, true)
      new_attractor_system.offset = 15
      new_attractor_system.max_distance = -1  // disable max_distance
      new_attractor_system.add_unaffected_attractor(this)
      new_attractor_system.add_attractor(new_attractor)
      this.attractor_systems_with_middle.push(new_attractor_system)
    }

    this.repelling_chance = this.repelling_chance_base
  }

  apply_force(force: p5.Vector) {
    if (this.repelling_end_frame > frameCount)
      super.apply_force(force.mult(1.5))
    else
      super.apply_force(force.mult(-0.75));
  }

  update() {
    // update position of the whole element
    super.update();

    // trigger random repelling event
    if (this.repelling_end_frame <= frameCount)
      if (random(0, 100) <= this.repelling_chance_base ) {
        this.repelling_end_frame = frameCount + floor(random(this.repelling_time_min, this.repelling_time_max))
        this.repelling_chance = this.repelling_chance_base
      } else {
        this.repelling_chance += this.repelling_chance_increase_per_frame
      }

    // push the satellite to the side and let it update it's position
    for (let i = 0; i < this.satellites.length; i++) {
      const s = this.satellites[i]

      s.nudge_to_point_after_dist_sq(this, 7225)

      // update before applying force from rotation to increase responsiveness when returning to center
      s.update()

      // use distance from position of this, rotate it and add it as force to the satellite to make them rotate
      let offset = Vector.sub(this.position, s.position)
      if (i % 2 == 0)
        offset.rotate(HALF_PI)
      else
        offset.rotate(-HALF_PI)

      offset.mult(0.05)
      s.apply_force(offset)
    }
  }

  draw() {
    push()
    stroke(0)
    for (let i = 0; i < this.satellites.length; i++) {
      const s = this.satellites[i]

      // draw point
      strokeWeight(5)
      point(s.position.x, s.position.y)

      // draw line to points close to this one
      for (let j = i + 1; j < this.satellites.length; j++) {
        const s2 = this.satellites[j]
        const distance = Vector.sub(s2.position, s.position)  // calculate vector from s to s2

        // using magSq instead of mag because it is faster but we need to compare against sqrt(distance)
        if (distance.magSq() < 400) {
          strokeWeight(1)
          line(s.position.x, s.position.y, s2.position.x, s2.position.y)
        }
      }
    }
    pop()
  }
}
