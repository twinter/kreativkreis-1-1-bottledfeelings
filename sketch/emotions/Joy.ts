class Joy extends Attractor {
  static satellites_amount: number = 7
  attractor_system_satellites: AttractorSystem
  attractor_systems: AttractorSystem[] = []
  satellites: Attractor[] = []

  constructor() {
    super()

    this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('joy_satellites', 0.5)
    this.attractor_system_satellites.max_distance = 2.5

    // add attractor objects and a separate system with the middle for each
    for (let i = 0; i < Joy.satellites_amount; i++) {
      // create new attractor
      const new_position = Vector.add(this.position, Vector.random2D().mult(15))
      const new_attractor = new Attractor(new_position.x, new_position.y)
      new_attractor.speed_max = 5
      new_attractor.speed_min = 0
      this.satellites.push(new_attractor)
      this.attractor_system_satellites.add_attractor(new_attractor)

      // create attractor system for the new attractor and the middle
      let new_attractor_system = AttractorSystem.get_new_attractor_system(`joy_satellite_${i}`, 1, true)
      new_attractor_system.max_distance = -1  // disable max_distance
      new_attractor_system.add_unaffected_attractor(this)
      new_attractor_system.add_attractor(new_attractor)
      this.attractor_systems.push(new_attractor_system)
    }
  }

  update() {
    // update position of the whole element
    super.update();

    for (const s of this.satellites) {
      s.nudge_to_point_after_dist_sq(this, 5625)
      s.update()
    }
  }

  draw() {
    push()
    stroke(0)
    strokeWeight(5)
    for (const s of this.satellites)
      point(s.position.x, s.position.y)
    pop()
  }
}
