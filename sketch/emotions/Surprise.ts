class Surprise extends Attractor {
  shape_amount: number = 3
  shape_edges_min: number = 3
  shape_edges_max: number = 6
  attractor_systems_with_middle: AttractorSystem[] = []
  shapes: Attractor[][] = []
  shape_attractor_systems: AttractorSystem[] = []
  // timing boundaries counted in frames
  time_to_live_min: number = 300
  time_to_live_max: number = 600
  time_to_reappear_min: number = 60
  time_to_reappear_max: number = 180
  private next_action_frame: number = 0
  private collapsing: boolean = false

  constructor() {
    super()

    this.appear()
  }

  private appear() {
    this.next_action_frame = frameCount + floor(random(this.time_to_live_min, this.time_to_live_max))

    // set position of whole object randomly
    this.position.x = random(0, width)
    this.position.y = random(0, height)

    this.generate_shapes()
  }

  private generate_shapes() {
    for (let i = 0; i < this.shape_amount; i++) {
      this.shapes[i] = []

      // generate attractor system for the shape
      const shape_attractor_system = AttractorSystem.get_new_attractor_system(`surprise_shape_${i}`, -25)
      this.shape_attractor_systems.push(shape_attractor_system)

      // generate a shape with a random amount of vertices
      const vertex_count = floor(random(this.shape_edges_min, this.shape_edges_max))
      for (let j = 0; j < vertex_count; j++) {
        // generate new point and add it to the shape and its attractor
        const offset = Vector.random2D()
        const new_position = Vector.add(this.position, offset)
        const new_attractor = new Attractor(new_position.x, new_position.y)
        this.shapes[i].push(new_attractor)
        shape_attractor_system.add_attractor(new_attractor)

        // create attractor system for this point and the middle
        const new_attractor_system = AttractorSystem.get_new_attractor_system(`surprise_shape_${i}_${j}`, 0.1, true)
        new_attractor_system.add_attractor(new_attractor)
        new_attractor_system.add_unaffected_attractor(this)
        this.attractor_systems_with_middle.push(new_attractor_system)
      }
    }
  }

  private disappear() {
    this.next_action_frame = frameCount + floor(random(this.time_to_reappear_min, this.time_to_reappear_max))

    for (const a of this.attractor_systems_with_middle)
      a.destroy()

    for (const s of this.shape_attractor_systems)
      s.destroy()

    this.shapes = []

    // set position to something far away to not influence the others
    this.position.x = -1000
    this.position.y = -1000
  }

  update() {
    // update position of the whole element
    super.update();

    // it is time to change something
    if (frameCount >= this.next_action_frame) {
      if (this.shapes.length > 0)
        // start collapsing or disappear
        if (this.collapsing) {
          this.disappear()
          this.collapsing = false
        } else {
          // start collapsing (push dots to the center)
          this.collapsing = true
          this.next_action_frame += 10
        }

      else
        this.appear()
    }

    for (const s of this.shapes) {
      for (const dot of s) {
        if (this.collapsing)
          dot.apply_force(Vector.sub(this.position, dot.position).mult(5))

        dot.update()
        dot.nudge_to_point_after_dist_sq(this, 7225)
      }
    }
  }

  draw() {
    push()
    stroke(0)

    // draw line connecting points of a shape
    strokeWeight(2)
    noFill()
    for (const s of this.shapes) {
      beginShape()
      for (const dot of s)
        vertex(dot.position.x, dot.position.y)
      endShape(CLOSE)
    }
    pop()
  }
}
