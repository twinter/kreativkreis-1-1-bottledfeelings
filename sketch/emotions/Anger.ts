class Anger extends Attractor {
  static satellites_amount = 7
  attractor_system_satellites: AttractorSystem
  attractor_systems_middle: AttractorSystem[] = []
  satellites: Attractor[] = []

  constructor() {
    super()

    // create attractor system that repels satellites from other satellites
    this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('anger_satellites', -10)
    this.attractor_system_satellites.max_distance = -1  // disable max_distance check for this system

    // create attractor and a system that attracts a satellite to the middle
    for (let i = 0; i < Anger.satellites_amount; i++) {
      const new_position = Vector.add(this.position, Vector.random2D())
      const new_attractor = new Attractor(new_position.x, new_position.y)
      new_attractor.speed_max = 5
      this.satellites.push(new_attractor)
      this.attractor_system_satellites.add_attractor(new_attractor)

      let new_attractor_system = AttractorSystem.get_new_attractor_system(`anger_sat_and_middle_${i}`, 1, true)
      new_attractor_system.offset = 2
      new_attractor_system.add_attractor(new_attractor)
      new_attractor_system.add_unaffected_attractor(this)
      this.attractor_systems_middle.push(new_attractor_system)
    }
  }

  update() {
    // update position of the whole element
    super.update();

    for (const s of this.satellites) {
      s.nudge_to_point_after_dist_sq(this, 7225)
      s.update()
    }

    // update attractor strength based on a random number
    const rand = noise(frameCount * 0.01, 10)
    if (rand < .55)
      this.attractor_system_satellites.strength = -10
    else
      this.attractor_system_satellites.strength = -1 - random(500, 5000)

  }

  draw() {
    push()
    stroke(0)
    strokeWeight(1)
    fill(64)
    beginShape()
    for (const s of this.satellites)
      vertex(s.position.x, s.position.y)
    endShape()
    pop()
  }
}
