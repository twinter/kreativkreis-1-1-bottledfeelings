class Fear extends Attractor {
  private dots_amount: number = 15
  private dot_displacement_max: number = 10

  constructor() {
    super();
    
    this.speed_max *= 1.25
  }

  apply_force(force: p5.Vector) {
    super.apply_force(force.mult(25));
  }

  draw() {
    push()
    stroke(0)
    strokeWeight(1)
    noFill()
    const diameter = this.dot_displacement_max * 2
    for (let i = 0; i < this.dots_amount; i++) {
      let offset = createVector(1, 0)

      // rotate vector randomly but smooth
      let rotation_strength = noise(frameCount * 0.01, 20, i) * 20
      offset.rotate(rotation_strength)

      // scale offset
      let length = noise(frameCount * 0.01, 25, i) * this.dot_displacement_max
      offset.mult(length)

      const dot = Vector.add(this.position, offset)
      circle(dot.x, dot.y, diameter)
    }
    pop()
  }
}
