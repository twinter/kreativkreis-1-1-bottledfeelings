class Sadness extends Attractor {
  private dots_amount: number = 15
  private dot_displacement_max: number = 15

  constructor() {
    super();

    this.speed_max *= 0.5
  }

  apply_force(force: p5.Vector) {
    super.apply_force(force.mult(0.5));
  }

  draw() {
    push()
    stroke(0)
    strokeWeight(this.dot_displacement_max * 2)
    for (let i = 0; i < this.dots_amount; i++) {
      let offset = createVector(1, 0);
      // set random orientation and distance from center
      offset.rotate(noise(frameCount * 0.002, 60, i) * 20)
      offset.mult(noise(frameCount * 0.004, 65, i) * this.dot_displacement_max)

      // stretch in the x axis to make it look more like a cloud
      offset.x *= 2.5

      const dot = Vector.add(this.position, offset)
      point(dot.x, dot.y)
    }
    pop()
  }
}
