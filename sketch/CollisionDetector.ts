interface Line {
  p1: p5.Vector;
  p2: p5.Vector;
}

class CollisionDetector {
  public static draw_debug: boolean = false;
  public static draw_boundaries: boolean = false;
  private static windowBoundaries: Line[] = []
  private static boundaries: Line[] = []

  static registerBoundaryLine(p1_or_x1: p5.Vector | number, p2_or_y1: p5.Vector | number, x2?: number, y2?: number) {
    if (x2 && y2 && typeof p1_or_x1 === 'number' && typeof p2_or_y1 === 'number')
      this.boundaries.push({p1: createVector(p1_or_x1, p2_or_y1), p2: createVector(x2, y2)});
    else
      this.boundaries.push({p1: <p5.Vector>p1_or_x1, p2: <p5.Vector>p2_or_y1});
  }

  static draw() {
    if (this.draw_boundaries)
      for (const b of this.boundaries)
        line(b.p1.x, b.p1.y, b.p2.x, b.p2.y)
  }

  /**
   * registers the window borders as boundary lines, can be called again to update them on window resize events
   */
  static registerWindowBoundaries() {
    this.windowBoundaries = []
    this.windowBoundaries.push({p1: createVector(0, 0), p2: createVector(width, 0)})  // top
    this.windowBoundaries.push({p1: createVector(0, height), p2: createVector(width, height)})  // bottom
    this.windowBoundaries.push({p1: createVector(0, 0), p2: createVector(0, height)})  // left
    this.windowBoundaries.push({p1: createVector(width, 0), p2: createVector(width, height)})  // right
  }

  /**
   * check if the given line intersects with any known boundaries. Return null if not or the vector reflected on the
   *  boundary if there is an intersection.
   */
  static getReflectedVector(l: Line): p5.Vector | null {
    const combinedBoundaries = this.boundaries.concat(this.windowBoundaries)
    for (const b of combinedBoundaries) {
      // @ts-ignore
      let intersection = collideLineLine(l.p1.x, l.p1.y, l.p2.x, l.p2.y, b.p1.x, b.p1.y, b.p2.x, b.p2.y, true)
      if (intersection.x !== false && intersection.y !== false) {
        // convert intersection point to a vector
        intersection = createVector(intersection.x, intersection.y)
        // we have a collision, calculate incoming and reflected vector
        let incomingVector = createVector(l.p2.x - l.p1.x, l.p2.y - l.p1.y)
        // create the normal by rotating the vector of the boundary line
        const normal_dx = b.p1.x - b.p2.x
        const normal_dy = b.p1.y - b.p2.y
        let normal = createVector(-normal_dy, normal_dx)
        normal.normalize()

        // flip the normal if it's pointing in the wrong direction (dot product <0 means the angle between normal and incoming vector is >90°)
        if (incomingVector.dot(normal) < 0)
          normal.mult(-1)

        // calculate the dot product
        const dot = incomingVector.dot(normal)

        // calculate and normalize reflected vector
        let reflectedVector = new Vector()
        reflectedVector.x = incomingVector.x - 2 * normal.x * dot
        reflectedVector.y = incomingVector.y - 2 * normal.y * dot

        // draw debug stuff if active
        if (this.draw_debug) {
          const normal_p2 = Vector.sub(intersection, Vector.mult(normal, 50))
          line(intersection.x, intersection.y, normal_p2.x, normal_p2.y)
          const direction_p2 = Vector.add(intersection, Vector.mult(reflectedVector, 25))
          line(intersection.x, intersection.y, direction_p2.x, direction_p2.y)
        }

        return reflectedVector
      }
    }

    return null
  }
}
