"use strict";
var Vector = p5.Vector;
var Attractor = /** @class */ (function () {
    function Attractor(pos_x, pos_y) {
        this.draw_enabled = false;
        this.speed_max = 3;
        this.speed_min = 0;
        this.position = createVector();
        this.position.x = pos_x !== null && pos_x !== void 0 ? pos_x : random(0, width);
        this.position.y = pos_y !== null && pos_y !== void 0 ? pos_y : random(0, height);
        // generate a unit vector in a random direction multiply to change the speed
        this.speed = Vector.random2D();
        this.speed.mult(random(1, 2));
    }
    Attractor.prototype.apply_force = function (force) {
        this.speed.add(force);
        this.speed.limit(this.speed_max);
        if (this.speed_min > 0) {
            var speed_abs = this.speed.mag();
            if (speed_abs < this.speed_min)
                this.speed.setMag(this.speed_min);
        }
    };
    /**
     * Moves this point a tiny bit to the target if the distance squared (!) is bigger than some limit.
     * We are comparing against the square of the distance because it is a lot faster and we will call this function very often.
     */
    Attractor.prototype.nudge_to_point_after_dist_sq = function (target, dist_sq) {
        var distance = Vector.sub(target.position, this.position);
        if (distance.magSq() > dist_sq)
            this.position.add(distance.mult(0.01));
    };
    Attractor.prototype.update = function () {
        // calculate new position
        var position_new = Vector.add(this.position, this.speed);
        // check if traveled line intersects with a boundary, get reflected vector if so
        var reflectedVector = CollisionDetector.getReflectedVector({ p1: this.position, p2: position_new });
        if (reflectedVector !== null) {
            this.speed = reflectedVector;
            position_new = Vector.add(this.position, this.speed);
        }
        // apply changed position
        this.position = position_new;
        // check if new position is outside of the window, respawn somewhere random if it is
        if (this.position.x < 0 || this.position.y < 0 || this.position.x > width || this.position.y > height) {
            this.position.x = random(0, width);
            this.position.y = random(0, height);
        }
    };
    Attractor.prototype.draw = function () {
        if (this.draw_enabled) {
            push();
            strokeWeight(5);
            point(this.position.x, this.position.y);
            pop();
        }
    };
    return Attractor;
}());
/**
 * This is a factory and manager for attractor systems at the same time.
 */
var AttractorSystem = /** @class */ (function () {
    // private constructor to prevent instantiation from outside
    function AttractorSystem(name, strength, inverted) {
        this.objects = [];
        this.objects_unaffected = [];
        // if the effect of the system is inverted (false: force decreases with distance, true: force increases with distance, disables max_distance)
        this.inverted = false;
        // reduce the calculated distance by a an offset, creates areas of least/most force in a circle with this radius
        this.offset = 0;
        // distance after which we stop calculating any forces to reduce required processing power
        this.max_distance = max(width, height) * 0.3;
        this.name = name;
        this.strength = strength;
        this.inverted = inverted;
    }
    /**
     * create a new attractor system of a specific name, the name has to be unique
     */
    AttractorSystem.get_new_attractor_system = function (name, strength, inverted) {
        if (inverted === void 0) { inverted = false; }
        // check if the name already exists
        if (this.attractor_systems.hasOwnProperty(name))
            throw new Error("there is already an attractor with the name \"" + name + "\" registered, can't register another one");
        // create new system, register it and return it
        var new_system = new AttractorSystem(name, strength, inverted);
        this.attractor_systems[name] = new_system;
        return new_system;
    };
    AttractorSystem.prototype.add_attractor = function (a) {
        this.objects.push(a);
    };
    /**
     * add an attractor to the system that affects other (regular) objects but is not affected by others in this system
     */
    AttractorSystem.prototype.add_unaffected_attractor = function (a) {
        this.objects_unaffected.push(a);
    };
    /**
     * Calculate the attraction (basically gravitational) force vector on the first object by the second one.
     * All masses are assumed to be 1.
     *
     * It uses the formula from https://physics.stackexchange.com/a/17291
     */
    AttractorSystem.prototype.calculate_force_on_object_by_object = function (o_target, o_cause) {
        // vector describing the distance
        var r = Vector.sub(o_cause.position, o_target.position);
        // scalar describing the distance
        var distance = r.mag();
        if (this.offset > 0) {
            distance -= this.offset;
            r.setMag(distance);
        }
        // return no force if objects are too far away
        if (this.max_distance > 0 && distance > this.max_distance && !this.inverted)
            return createVector(0, 0);
        if (this.inverted) {
            // calculate the force with f = strength * r * distance, strength is reduced so we don't have to use tiny numbers
            var f = Vector.mult(r, this.strength / 1000);
            f.mult(distance);
            return f;
        }
        else {
            // calculate the force with f = (strength * r)/distance^3
            var f = Vector.mult(r, this.strength);
            f.div(Math.pow(distance, 3));
            return f;
        }
    };
    /**
     * destroy this attractor system, no more updates will happen and the name can be assigned again
     */
    AttractorSystem.prototype.destroy = function () {
        delete AttractorSystem.attractor_systems[this.name];
    };
    /**
     * update all attractor systems
     */
    AttractorSystem.update = function () {
        for (var name_1 in this.attractor_systems)
            this.attractor_systems[name_1]._update();
    };
    /**
     * calculate and apply forces from the system, don't update the objects here to avoid double updates in case it is in more than one system
     */
    AttractorSystem.prototype._update = function () {
        // calculate and apply forces from unaffected objects on regular objects
        for (var _i = 0, _a = this.objects_unaffected; _i < _a.length; _i++) {
            var o = _a[_i];
            for (var _b = 0, _c = this.objects; _b < _c.length; _b++) {
                var o2 = _c[_b];
                o2.apply_force(this.calculate_force_on_object_by_object(o2, o));
            }
        }
        // calculate forces between regular objects
        for (var i = 0; i < this.objects.length; i++) {
            for (var j = i + 1; j < this.objects.length; j++) {
                var o1 = this.objects[i];
                var o2 = this.objects[j];
                var force = this.calculate_force_on_object_by_object(o1, o2);
                o1.apply_force(force);
                o2.apply_force(force.mult(-1));
            }
        }
    };
    AttractorSystem.prototype.apply_one_time_force = function (position, strength) {
        if (strength === void 0) { strength = this.strength; }
        // save original strength so we can reset it when we're done and replace it with the strength given as argument
        var strength_orig = this.strength;
        this.strength = strength;
        // create a (temporary) fake attractor
        var force_origin = new Attractor(position.x, position.y);
        // calculate and apply forces to objects
        for (var _i = 0, _a = this.objects; _i < _a.length; _i++) {
            var o = _a[_i];
            var force = this.calculate_force_on_object_by_object(o, force_origin);
            o.apply_force(force);
        }
        // restore original strength
        this.strength = strength_orig;
    };
    // class attributes (i.e. attributes of the factory)
    // a list of all systems
    AttractorSystem.attractor_systems = {};
    return AttractorSystem;
}());
var CollisionDetector = /** @class */ (function () {
    function CollisionDetector() {
    }
    CollisionDetector.registerBoundaryLine = function (p1_or_x1, p2_or_y1, x2, y2) {
        if (x2 && y2 && typeof p1_or_x1 === 'number' && typeof p2_or_y1 === 'number')
            this.boundaries.push({ p1: createVector(p1_or_x1, p2_or_y1), p2: createVector(x2, y2) });
        else
            this.boundaries.push({ p1: p1_or_x1, p2: p2_or_y1 });
    };
    CollisionDetector.draw = function () {
        if (this.draw_boundaries)
            for (var _i = 0, _a = this.boundaries; _i < _a.length; _i++) {
                var b = _a[_i];
                line(b.p1.x, b.p1.y, b.p2.x, b.p2.y);
            }
    };
    /**
     * registers the window borders as boundary lines, can be called again to update them on window resize events
     */
    CollisionDetector.registerWindowBoundaries = function () {
        this.windowBoundaries = [];
        this.windowBoundaries.push({ p1: createVector(0, 0), p2: createVector(width, 0) }); // top
        this.windowBoundaries.push({ p1: createVector(0, height), p2: createVector(width, height) }); // bottom
        this.windowBoundaries.push({ p1: createVector(0, 0), p2: createVector(0, height) }); // left
        this.windowBoundaries.push({ p1: createVector(width, 0), p2: createVector(width, height) }); // right
    };
    /**
     * check if the given line intersects with any known boundaries. Return null if not or the vector reflected on the
     *  boundary if there is an intersection.
     */
    CollisionDetector.getReflectedVector = function (l) {
        var combinedBoundaries = this.boundaries.concat(this.windowBoundaries);
        for (var _i = 0, combinedBoundaries_1 = combinedBoundaries; _i < combinedBoundaries_1.length; _i++) {
            var b = combinedBoundaries_1[_i];
            // @ts-ignore
            var intersection = collideLineLine(l.p1.x, l.p1.y, l.p2.x, l.p2.y, b.p1.x, b.p1.y, b.p2.x, b.p2.y, true);
            if (intersection.x !== false && intersection.y !== false) {
                // convert intersection point to a vector
                intersection = createVector(intersection.x, intersection.y);
                // we have a collision, calculate incoming and reflected vector
                var incomingVector = createVector(l.p2.x - l.p1.x, l.p2.y - l.p1.y);
                // create the normal by rotating the vector of the boundary line
                var normal_dx = b.p1.x - b.p2.x;
                var normal_dy = b.p1.y - b.p2.y;
                var normal = createVector(-normal_dy, normal_dx);
                normal.normalize();
                // flip the normal if it's pointing in the wrong direction (dot product <0 means the angle between normal and incoming vector is >90°)
                if (incomingVector.dot(normal) < 0)
                    normal.mult(-1);
                // calculate the dot product
                var dot = incomingVector.dot(normal);
                // calculate and normalize reflected vector
                var reflectedVector = new Vector();
                reflectedVector.x = incomingVector.x - 2 * normal.x * dot;
                reflectedVector.y = incomingVector.y - 2 * normal.y * dot;
                // draw debug stuff if active
                if (this.draw_debug) {
                    var normal_p2 = Vector.sub(intersection, Vector.mult(normal, 50));
                    line(intersection.x, intersection.y, normal_p2.x, normal_p2.y);
                    var direction_p2 = Vector.add(intersection, Vector.mult(reflectedVector, 25));
                    line(intersection.x, intersection.y, direction_p2.x, direction_p2.y);
                }
                return reflectedVector;
            }
        }
        return null;
    };
    CollisionDetector.draw_debug = false;
    CollisionDetector.draw_boundaries = false;
    CollisionDetector.windowBoundaries = [];
    CollisionDetector.boundaries = [];
    return CollisionDetector;
}());
///<reference path="AttractorSystem.ts"/>
var global_attractor;
var bottle;
var objects;
var paused = false;
var mouse_attractor;
function setup() {
    setupScreen();
    bottle = new Bottle();
    // create global attractor system
    global_attractor = AttractorSystem.get_new_attractor_system('global', -100);
    global_attractor.max_distance = -1;
    // create mouse attractor and add it to the system
    mouse_attractor = new Attractor(mouseX, mouseY);
    global_attractor.add_unaffected_attractor(mouse_attractor);
    // add emotions
    objects = [];
    for (var _i = 0, _a = [new Anger(), new Fear(), new Joy(), new Love(), new Sadness(), new Surprise()]; _i < _a.length; _i++) {
        var e = _a[_i];
        objects.push(e);
        global_attractor.add_attractor(e);
    }
}
function draw() {
    // update stuff
    bottle.update();
    mouse_attractor.position.x = mouseX;
    mouse_attractor.position.y = mouseY;
    AttractorSystem.update();
    for (var _i = 0, objects_1 = objects; _i < objects_1.length; _i++) {
        var o = objects_1[_i];
        o.update();
    }
    // draw stuff
    background(201);
    bottle.draw();
    CollisionDetector.draw(); // used to draw boundaries if enabled
    for (var _a = 0, objects_2 = objects; _a < objects_2.length; _a++) {
        var o = objects_2[_a];
        o.draw();
    }
}
/**
 * p5 builtin function to handle window resize events
 */
function windowResized() {
    setupScreen();
}
function setupScreen() {
    createCanvas(windowWidth, windowHeight);
    CollisionDetector.registerWindowBoundaries();
}
function pause() {
    paused = true;
    noLoop();
    console.log('loop paused');
}
function unpause() {
    paused = false;
    loop();
    console.log('resuming loop');
}
function toggle_pause() {
    if (paused)
        unpause();
    else
        pause();
}
/**
 * p5 builtin function to handle key press events
 */
function keyPressed() {
    if (keyCode === ESCAPE)
        pause();
    else if (key === ' ')
        toggle_pause();
}
function mouseClicked() {
    if (!paused)
        global_attractor.apply_one_time_force(createVector(mouseX, mouseY), -10000);
}
var Bottle = /** @class */ (function () {
    function Bottle() {
        this.lines = [];
        // define vectors of the relevant points
        var body_bottom_left = createVector(width / 4, height - 25);
        var body_bottom_right = createVector((3 / 4) * width, height - 25);
        var body_top_left = createVector(width / 4, height / 2);
        var body_top_right = createVector((3 / 4) * width, height / 2);
        var opening_bottom_left = createVector((3 / 8) * width, (3 / 8) * height);
        var opening_bottom_right = createVector((5 / 8) * width, (3 / 8) * height);
        var opening_top_left = createVector((3 / 8) * width, (2 / 8) * height);
        var opening_top_right = createVector((5 / 8) * width, (2 / 8) * height);
        // create lines
        this.lines.push(new BottleLine(opening_top_left, opening_bottom_left));
        this.lines.push(new BottleLine(opening_bottom_left, body_top_left));
        this.lines.push(new BottleLine(body_top_left, body_bottom_left));
        this.lines.push(new BottleLine(body_bottom_left, body_bottom_right));
        this.lines.push(new BottleLine(body_bottom_right, body_top_right));
        this.lines.push(new BottleLine(body_top_right, opening_bottom_right));
        this.lines.push(new BottleLine(opening_bottom_right, opening_top_right));
    }
    Bottle.prototype.update = function () {
        for (var _i = 0, _a = this.lines; _i < _a.length; _i++) {
            var l = _a[_i];
            l.update();
        }
    };
    Bottle.prototype.draw = function () {
        for (var _i = 0, _a = this.lines; _i < _a.length; _i++) {
            var l = _a[_i];
            l.draw();
        }
    };
    return Bottle;
}());
var BottleLine = /** @class */ (function () {
    function BottleLine(p1, p2) {
        this.dot_distance = 20;
        this.dot_displacement_max = 15;
        this.lines_amount = 5;
        this.p1 = p1;
        this.p2 = p2;
        CollisionDetector.registerBoundaryLine(p1.x, p1.y, p2.x, p2.y);
    }
    BottleLine.prototype.update = function () {
    };
    BottleLine.prototype.draw = function () {
        push();
        stroke(0);
        strokeWeight(3);
        noFill();
        // spread dots evenly along the line with this.dot_distance as target, calculate real distance between dots based on that
        var distance = Vector.sub(this.p1, this.p2).mag();
        var dots_to_place = round(distance / this.dot_distance);
        var dot_distance_adapted = distance / dots_to_place;
        // get a unit vector pointing in the direction of the line so we can easily place the dots using vectors
        var line_direction = Vector.sub(this.p2, this.p1).normalize();
        for (var i = 0; i < this.lines_amount; i++) {
            beginShape();
            // place dots and displace them randomly, we place dots_to_place + 1 dots to get one on both ends
            for (var j = 0; j <= dots_to_place; j++) {
                var dot_pos = this.p1.copy();
                dot_pos.add(Vector.mult(line_direction, j * dot_distance_adapted));
                var offset = createVector();
                offset.x = noise((dot_pos.x + i) * 0.001, (dot_pos.y + j) * 0.001, frameCount * 0.01) * 10;
                offset.y = noise((dot_pos.x + i) * 0.001, (dot_pos.y + j) * 0.001, 100 + frameCount * 0.01) * 10;
                offset.normalize();
                offset.mult(noise(dot_pos.x + i, dot_pos.y + j, frameCount * 0.01) * this.dot_displacement_max);
                dot_pos.add(offset);
                vertex(dot_pos.x, dot_pos.y);
            }
            endShape();
        }
        pop();
    };
    return BottleLine;
}());
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Anger = /** @class */ (function (_super) {
    __extends(Anger, _super);
    function Anger() {
        var _this = _super.call(this) || this;
        _this.attractor_systems_middle = [];
        _this.satellites = [];
        // create attractor system that repels satellites from other satellites
        _this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('anger_satellites', -10);
        _this.attractor_system_satellites.max_distance = -1; // disable max_distance check for this system
        // create attractor and a system that attracts a satellite to the middle
        for (var i = 0; i < Anger.satellites_amount; i++) {
            var new_position = Vector.add(_this.position, Vector.random2D());
            var new_attractor = new Attractor(new_position.x, new_position.y);
            new_attractor.speed_max = 5;
            _this.satellites.push(new_attractor);
            _this.attractor_system_satellites.add_attractor(new_attractor);
            var new_attractor_system = AttractorSystem.get_new_attractor_system("anger_sat_and_middle_" + i, 1, true);
            new_attractor_system.offset = 2;
            new_attractor_system.add_attractor(new_attractor);
            new_attractor_system.add_unaffected_attractor(_this);
            _this.attractor_systems_middle.push(new_attractor_system);
        }
        return _this;
    }
    Anger.prototype.update = function () {
        // update position of the whole element
        _super.prototype.update.call(this);
        for (var _i = 0, _a = this.satellites; _i < _a.length; _i++) {
            var s = _a[_i];
            s.nudge_to_point_after_dist_sq(this, 7225);
            s.update();
        }
        // update attractor strength based on a random number
        var rand = noise(frameCount * 0.01, 10);
        if (rand < .55)
            this.attractor_system_satellites.strength = -10;
        else
            this.attractor_system_satellites.strength = -1 - random(500, 5000);
    };
    Anger.prototype.draw = function () {
        push();
        stroke(0);
        strokeWeight(1);
        fill(64);
        beginShape();
        for (var _i = 0, _a = this.satellites; _i < _a.length; _i++) {
            var s = _a[_i];
            vertex(s.position.x, s.position.y);
        }
        endShape();
        pop();
    };
    Anger.satellites_amount = 7;
    return Anger;
}(Attractor));
var Fear = /** @class */ (function (_super) {
    __extends(Fear, _super);
    function Fear() {
        var _this = _super.call(this) || this;
        _this.dots_amount = 15;
        _this.dot_displacement_max = 10;
        _this.speed_max *= 1.25;
        return _this;
    }
    Fear.prototype.apply_force = function (force) {
        _super.prototype.apply_force.call(this, force.mult(25));
    };
    Fear.prototype.draw = function () {
        push();
        stroke(0);
        strokeWeight(1);
        noFill();
        var diameter = this.dot_displacement_max * 2;
        for (var i = 0; i < this.dots_amount; i++) {
            var offset = createVector(1, 0);
            // rotate vector randomly but smooth
            var rotation_strength = noise(frameCount * 0.01, 20, i) * 20;
            offset.rotate(rotation_strength);
            // scale offset
            var length_1 = noise(frameCount * 0.01, 25, i) * this.dot_displacement_max;
            offset.mult(length_1);
            var dot = Vector.add(this.position, offset);
            circle(dot.x, dot.y, diameter);
        }
        pop();
    };
    return Fear;
}(Attractor));
var Joy = /** @class */ (function (_super) {
    __extends(Joy, _super);
    function Joy() {
        var _this = _super.call(this) || this;
        _this.attractor_systems = [];
        _this.satellites = [];
        _this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('joy_satellites', 0.5);
        _this.attractor_system_satellites.max_distance = 2.5;
        // add attractor objects and a separate system with the middle for each
        for (var i = 0; i < Joy.satellites_amount; i++) {
            // create new attractor
            var new_position = Vector.add(_this.position, Vector.random2D().mult(15));
            var new_attractor = new Attractor(new_position.x, new_position.y);
            new_attractor.speed_max = 5;
            new_attractor.speed_min = 0;
            _this.satellites.push(new_attractor);
            _this.attractor_system_satellites.add_attractor(new_attractor);
            // create attractor system for the new attractor and the middle
            var new_attractor_system = AttractorSystem.get_new_attractor_system("joy_satellite_" + i, 1, true);
            new_attractor_system.max_distance = -1; // disable max_distance
            new_attractor_system.add_unaffected_attractor(_this);
            new_attractor_system.add_attractor(new_attractor);
            _this.attractor_systems.push(new_attractor_system);
        }
        return _this;
    }
    Joy.prototype.update = function () {
        // update position of the whole element
        _super.prototype.update.call(this);
        for (var _i = 0, _a = this.satellites; _i < _a.length; _i++) {
            var s = _a[_i];
            s.nudge_to_point_after_dist_sq(this, 5625);
            s.update();
        }
    };
    Joy.prototype.draw = function () {
        push();
        stroke(0);
        strokeWeight(5);
        for (var _i = 0, _a = this.satellites; _i < _a.length; _i++) {
            var s = _a[_i];
            point(s.position.x, s.position.y);
        }
        pop();
    };
    Joy.satellites_amount = 7;
    return Joy;
}(Attractor));
var Love = /** @class */ (function (_super) {
    __extends(Love, _super);
    function Love() {
        var _this = _super.call(this) || this;
        _this.attractor_systems_with_middle = [];
        _this.satellites = [];
        _this.repelling_end_frame = 0; // number of frame when repelling ends
        _this.repelling_time_min = 30;
        _this.repelling_time_max = 180;
        _this.repelling_chance_base = 0.1; // chance to start repelling others in %
        _this.repelling_chance_increase_per_frame = 0.1;
        // add attractor object that repels satellites from each other
        _this.attractor_system_satellites = AttractorSystem.get_new_attractor_system('love_satellites', -35);
        for (var i = 0; i < Love.satellites_amount; i++) {
            // create new attractor
            var offset = Vector.random2D().mult(15);
            var new_position = Vector.add(_this.position, offset);
            var new_attractor = new Attractor(new_position.x, new_position.y);
            _this.satellites.push(new_attractor);
            // add to system that repels satellites from each other
            _this.attractor_system_satellites.add_attractor(new_attractor);
            // create attractor with middle
            var new_attractor_system = AttractorSystem.get_new_attractor_system("love_satellite_" + i, 15, true);
            new_attractor_system.offset = 15;
            new_attractor_system.max_distance = -1; // disable max_distance
            new_attractor_system.add_unaffected_attractor(_this);
            new_attractor_system.add_attractor(new_attractor);
            _this.attractor_systems_with_middle.push(new_attractor_system);
        }
        _this.repelling_chance = _this.repelling_chance_base;
        return _this;
    }
    Love.prototype.apply_force = function (force) {
        if (this.repelling_end_frame > frameCount)
            _super.prototype.apply_force.call(this, force.mult(1.5));
        else
            _super.prototype.apply_force.call(this, force.mult(-0.75));
    };
    Love.prototype.update = function () {
        // update position of the whole element
        _super.prototype.update.call(this);
        // trigger random repelling event
        if (this.repelling_end_frame <= frameCount)
            if (random(0, 100) <= this.repelling_chance_base) {
                this.repelling_end_frame = frameCount + floor(random(this.repelling_time_min, this.repelling_time_max));
                this.repelling_chance = this.repelling_chance_base;
            }
            else {
                this.repelling_chance += this.repelling_chance_increase_per_frame;
            }
        // push the satellite to the side and let it update it's position
        for (var i = 0; i < this.satellites.length; i++) {
            var s = this.satellites[i];
            s.nudge_to_point_after_dist_sq(this, 7225);
            // update before applying force from rotation to increase responsiveness when returning to center
            s.update();
            // use distance from position of this, rotate it and add it as force to the satellite to make them rotate
            var offset = Vector.sub(this.position, s.position);
            if (i % 2 == 0)
                offset.rotate(HALF_PI);
            else
                offset.rotate(-HALF_PI);
            offset.mult(0.05);
            s.apply_force(offset);
        }
    };
    Love.prototype.draw = function () {
        push();
        stroke(0);
        for (var i = 0; i < this.satellites.length; i++) {
            var s = this.satellites[i];
            // draw point
            strokeWeight(5);
            point(s.position.x, s.position.y);
            // draw line to points close to this one
            for (var j = i + 1; j < this.satellites.length; j++) {
                var s2 = this.satellites[j];
                var distance = Vector.sub(s2.position, s.position); // calculate vector from s to s2
                // using magSq instead of mag because it is faster but we need to compare against sqrt(distance)
                if (distance.magSq() < 400) {
                    strokeWeight(1);
                    line(s.position.x, s.position.y, s2.position.x, s2.position.y);
                }
            }
        }
        pop();
    };
    Love.satellites_amount = 10;
    return Love;
}(Attractor));
var Sadness = /** @class */ (function (_super) {
    __extends(Sadness, _super);
    function Sadness() {
        var _this = _super.call(this) || this;
        _this.dots_amount = 15;
        _this.dot_displacement_max = 15;
        _this.speed_max *= 0.5;
        return _this;
    }
    Sadness.prototype.apply_force = function (force) {
        _super.prototype.apply_force.call(this, force.mult(0.5));
    };
    Sadness.prototype.draw = function () {
        push();
        stroke(0);
        strokeWeight(this.dot_displacement_max * 2);
        for (var i = 0; i < this.dots_amount; i++) {
            var offset = createVector(1, 0);
            // set random orientation and distance from center
            offset.rotate(noise(frameCount * 0.002, 60, i) * 20);
            offset.mult(noise(frameCount * 0.004, 65, i) * this.dot_displacement_max);
            // stretch in the x axis to make it look more like a cloud
            offset.x *= 2.5;
            var dot = Vector.add(this.position, offset);
            point(dot.x, dot.y);
        }
        pop();
    };
    return Sadness;
}(Attractor));
var Surprise = /** @class */ (function (_super) {
    __extends(Surprise, _super);
    function Surprise() {
        var _this = _super.call(this) || this;
        _this.shape_amount = 3;
        _this.shape_edges_min = 3;
        _this.shape_edges_max = 6;
        _this.attractor_systems_with_middle = [];
        _this.shapes = [];
        _this.shape_attractor_systems = [];
        // timing boundaries counted in frames
        _this.time_to_live_min = 300;
        _this.time_to_live_max = 600;
        _this.time_to_reappear_min = 60;
        _this.time_to_reappear_max = 180;
        _this.next_action_frame = 0;
        _this.collapsing = false;
        _this.appear();
        return _this;
    }
    Surprise.prototype.appear = function () {
        this.next_action_frame = frameCount + floor(random(this.time_to_live_min, this.time_to_live_max));
        // set position of whole object randomly
        this.position.x = random(0, width);
        this.position.y = random(0, height);
        this.generate_shapes();
    };
    Surprise.prototype.generate_shapes = function () {
        for (var i = 0; i < this.shape_amount; i++) {
            this.shapes[i] = [];
            // generate attractor system for the shape
            var shape_attractor_system = AttractorSystem.get_new_attractor_system("surprise_shape_" + i, -25);
            this.shape_attractor_systems.push(shape_attractor_system);
            // generate a shape with a random amount of vertices
            var vertex_count = floor(random(this.shape_edges_min, this.shape_edges_max));
            for (var j = 0; j < vertex_count; j++) {
                // generate new point and add it to the shape and its attractor
                var offset = Vector.random2D();
                var new_position = Vector.add(this.position, offset);
                var new_attractor = new Attractor(new_position.x, new_position.y);
                this.shapes[i].push(new_attractor);
                shape_attractor_system.add_attractor(new_attractor);
                // create attractor system for this point and the middle
                var new_attractor_system = AttractorSystem.get_new_attractor_system("surprise_shape_" + i + "_" + j, 0.1, true);
                new_attractor_system.add_attractor(new_attractor);
                new_attractor_system.add_unaffected_attractor(this);
                this.attractor_systems_with_middle.push(new_attractor_system);
            }
        }
    };
    Surprise.prototype.disappear = function () {
        this.next_action_frame = frameCount + floor(random(this.time_to_reappear_min, this.time_to_reappear_max));
        for (var _i = 0, _a = this.attractor_systems_with_middle; _i < _a.length; _i++) {
            var a = _a[_i];
            a.destroy();
        }
        for (var _b = 0, _c = this.shape_attractor_systems; _b < _c.length; _b++) {
            var s = _c[_b];
            s.destroy();
        }
        this.shapes = [];
        // set position to something far away to not influence the others
        this.position.x = -1000;
        this.position.y = -1000;
    };
    Surprise.prototype.update = function () {
        // update position of the whole element
        _super.prototype.update.call(this);
        // it is time to change something
        if (frameCount >= this.next_action_frame) {
            if (this.shapes.length > 0)
                // start collapsing or disappear
                if (this.collapsing) {
                    this.disappear();
                    this.collapsing = false;
                }
                else {
                    // start collapsing (push dots to the center)
                    this.collapsing = true;
                    this.next_action_frame += 10;
                }
            else
                this.appear();
        }
        for (var _i = 0, _a = this.shapes; _i < _a.length; _i++) {
            var s = _a[_i];
            for (var _b = 0, s_1 = s; _b < s_1.length; _b++) {
                var dot = s_1[_b];
                if (this.collapsing)
                    dot.apply_force(Vector.sub(this.position, dot.position).mult(5));
                dot.update();
                dot.nudge_to_point_after_dist_sq(this, 7225);
            }
        }
    };
    Surprise.prototype.draw = function () {
        push();
        stroke(0);
        // draw line connecting points of a shape
        strokeWeight(2);
        noFill();
        for (var _i = 0, _a = this.shapes; _i < _a.length; _i++) {
            var s = _a[_i];
            beginShape();
            for (var _b = 0, s_2 = s; _b < s_2.length; _b++) {
                var dot = s_2[_b];
                vertex(dot.position.x, dot.position.y);
            }
            endShape(CLOSE);
        }
        pop();
    };
    return Surprise;
}(Attractor));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2tldGNoLmpzIiwic291cmNlUm9vdCI6Ii4vc2tldGNoLyIsInNvdXJjZXMiOlsiQXR0cmFjdG9yLnRzIiwiQXR0cmFjdG9yU3lzdGVtLnRzIiwiQ29sbGlzaW9uRGV0ZWN0b3IudHMiLCJza2V0Y2gudHMiLCJib3R0bGUvQm90dGxlLnRzIiwiYm90dGxlL0JvdHRsZUxpbmUudHMiLCJlbW90aW9ucy9Bbmdlci50cyIsImVtb3Rpb25zL0ZlYXIudHMiLCJlbW90aW9ucy9Kb3kudHMiLCJlbW90aW9ucy9Mb3ZlLnRzIiwiZW1vdGlvbnMvU2FkbmVzcy50cyIsImVtb3Rpb25zL1N1cnByaXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxJQUFNLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFBO0FBRXhCO0lBT0UsbUJBQVksS0FBYyxFQUFFLEtBQWM7UUFMMUMsaUJBQVksR0FBWSxLQUFLLENBQUE7UUFFdEIsY0FBUyxHQUFXLENBQUMsQ0FBQTtRQUNyQixjQUFTLEdBQVcsQ0FBQyxDQUFBO1FBRzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxFQUFFLENBQUE7UUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxhQUFMLEtBQUssY0FBTCxLQUFLLEdBQUksTUFBTSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxLQUFLLGFBQUwsS0FBSyxjQUFMLEtBQUssR0FBSSxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBRTVDLDRFQUE0RTtRQUM1RSxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQTtRQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDL0IsQ0FBQztJQUVELCtCQUFXLEdBQVgsVUFBWSxLQUFnQjtRQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFaEMsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRTtZQUN0QixJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFBO1lBQ2xDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTO2dCQUM1QixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7U0FDcEM7SUFDSCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsZ0RBQTRCLEdBQTVCLFVBQTZCLE1BQWlCLEVBQUUsT0FBZTtRQUM3RCxJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQzNELElBQUksUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLE9BQU87WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBQzFDLENBQUM7SUFHRCwwQkFBTSxHQUFOO1FBQ0UseUJBQXlCO1FBQ3pCLElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFeEQsZ0ZBQWdGO1FBQ2hGLElBQU0sZUFBZSxHQUFHLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLEVBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBQyxDQUFDLENBQUE7UUFDbkcsSUFBSSxlQUFlLEtBQUssSUFBSSxFQUFFO1lBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFBO1lBQzVCLFlBQVksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3JEO1FBRUQseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFBO1FBRTVCLG9GQUFvRjtRQUNwRixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLE1BQU0sRUFBRTtZQUNyRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUE7U0FDcEM7SUFDSCxDQUFDO0lBRUQsd0JBQUksR0FBSjtRQUNFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLEVBQUUsQ0FBQTtZQUNOLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNmLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ3ZDLEdBQUcsRUFBRSxDQUFBO1NBQ047SUFDSCxDQUFDO0lBQ0gsZ0JBQUM7QUFBRCxDQUFDLEFBcEVELElBb0VDO0FDdEVEOztHQUVHO0FBQ0g7SUFrQkUsNERBQTREO0lBQzVELHlCQUFvQixJQUFZLEVBQUUsUUFBZ0IsRUFBRSxRQUFpQjtRQVo3RCxZQUFPLEdBQWdCLEVBQUUsQ0FBQTtRQUN6Qix1QkFBa0IsR0FBZ0IsRUFBRSxDQUFBO1FBQzVDLDZJQUE2STtRQUM3SSxhQUFRLEdBQVksS0FBSyxDQUFBO1FBQ3pCLGdIQUFnSDtRQUNoSCxXQUFNLEdBQVcsQ0FBQyxDQUFBO1FBR2xCLDBGQUEwRjtRQUMxRixpQkFBWSxHQUFXLEdBQUcsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFBO1FBSTdDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO0lBQzFCLENBQUM7SUFFRDs7T0FFRztJQUNJLHdDQUF3QixHQUEvQixVQUFnQyxJQUFZLEVBQUUsUUFBZ0IsRUFBRSxRQUF5QjtRQUF6Qix5QkFBQSxFQUFBLGdCQUF5QjtRQUN2RixtQ0FBbUM7UUFDbkMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztZQUM3QyxNQUFNLElBQUksS0FBSyxDQUFDLG1EQUFnRCxJQUFJLDhDQUEwQyxDQUFDLENBQUE7UUFFakgsK0NBQStDO1FBQy9DLElBQU0sVUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUE7UUFDaEUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxHQUFHLFVBQVUsQ0FBQTtRQUN6QyxPQUFPLFVBQVUsQ0FBQTtJQUNuQixDQUFDO0lBRUQsdUNBQWEsR0FBYixVQUFjLENBQVk7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsa0RBQXdCLEdBQXhCLFVBQXlCLENBQVk7UUFDbkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNqQyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSyw2REFBbUMsR0FBM0MsVUFBNEMsUUFBbUIsRUFBRSxPQUFrQjtRQUNqRixpQ0FBaUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUN2RCxpQ0FBaUM7UUFDakMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFBO1FBRXRCLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkIsUUFBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUE7WUFDdkIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTtTQUNuQjtRQUVELDhDQUE4QztRQUM5QyxJQUFJLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDekUsT0FBTyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBRTNCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixpSEFBaUg7WUFDakgsSUFBSSxDQUFDLEdBQWMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQTtZQUN2RCxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQ2hCLE9BQU8sQ0FBQyxDQUFBO1NBQ1Q7YUFBTTtZQUNMLHlEQUF5RDtZQUN6RCxJQUFJLENBQUMsR0FBYyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDaEQsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQzVCLE9BQU8sQ0FBQyxDQUFBO1NBQ1Q7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxpQ0FBTyxHQUFQO1FBQ0UsT0FBTyxlQUFlLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQ3JELENBQUM7SUFFRDs7T0FFRztJQUNJLHNCQUFNLEdBQWI7UUFDRSxLQUFLLElBQU0sTUFBSSxJQUFJLElBQUksQ0FBQyxpQkFBaUI7WUFDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFBO0lBQzFDLENBQUM7SUFFRDs7T0FFRztJQUNLLGlDQUFPLEdBQWY7UUFDRSx3RUFBd0U7UUFDeEUsS0FBZ0IsVUFBdUIsRUFBdkIsS0FBQSxJQUFJLENBQUMsa0JBQWtCLEVBQXZCLGNBQXVCLEVBQXZCLElBQXVCLEVBQUU7WUFBcEMsSUFBTSxDQUFDLFNBQUE7WUFDVixLQUFpQixVQUFZLEVBQVosS0FBQSxJQUFJLENBQUMsT0FBTyxFQUFaLGNBQVksRUFBWixJQUFZO2dCQUF4QixJQUFNLEVBQUUsU0FBQTtnQkFDWCxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUFBO1NBQ2xFO1FBRUQsMkNBQTJDO1FBQzNDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNoRCxJQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMxQixJQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMxQixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsbUNBQW1DLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFBO2dCQUM5RCxFQUFFLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUNyQixFQUFFLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQy9CO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsOENBQW9CLEdBQXBCLFVBQXFCLFFBQW1CLEVBQUUsUUFBZ0M7UUFBaEMseUJBQUEsRUFBQSxXQUFtQixJQUFJLENBQUMsUUFBUTtRQUN4RSwrR0FBK0c7UUFDL0csSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQTtRQUNuQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQTtRQUV4QixzQ0FBc0M7UUFDdEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFFeEQsd0NBQXdDO1FBQ3hDLEtBQWdCLFVBQVksRUFBWixLQUFBLElBQUksQ0FBQyxPQUFPLEVBQVosY0FBWSxFQUFaLElBQVksRUFBRTtZQUF6QixJQUFNLENBQUMsU0FBQTtZQUNWLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLEVBQUcsWUFBWSxDQUFDLENBQUE7WUFDeEUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNyQjtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQTtJQUMvQixDQUFDO0lBeElELG9EQUFvRDtJQUNwRCx3QkFBd0I7SUFDVCxpQ0FBaUIsR0FBd0MsRUFBRSxDQUFBO0lBdUk1RSxzQkFBQztDQUFBLEFBMUlELElBMElDO0FDeElEO0lBQUE7SUE0RUEsQ0FBQztJQXRFUSxzQ0FBb0IsR0FBM0IsVUFBNEIsUUFBNEIsRUFBRSxRQUE0QixFQUFFLEVBQVcsRUFBRSxFQUFXO1FBQzlHLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxPQUFPLFFBQVEsS0FBSyxRQUFRLElBQUksT0FBTyxRQUFRLEtBQUssUUFBUTtZQUMxRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQzs7WUFFdkYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBQyxFQUFFLEVBQWEsUUFBUSxFQUFFLEVBQUUsRUFBYSxRQUFRLEVBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFTSxzQkFBSSxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZTtZQUN0QixLQUFnQixVQUFlLEVBQWYsS0FBQSxJQUFJLENBQUMsVUFBVSxFQUFmLGNBQWUsRUFBZixJQUFlO2dCQUExQixJQUFNLENBQUMsU0FBQTtnQkFDVixJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUFBO0lBQzFDLENBQUM7SUFFRDs7T0FFRztJQUNJLDBDQUF3QixHQUEvQjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7UUFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQSxDQUFFLE1BQU07UUFDeEYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxFQUFDLENBQUMsQ0FBQSxDQUFFLFNBQVM7UUFDckcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxFQUFDLENBQUMsQ0FBQSxDQUFFLE9BQU87UUFDMUYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxFQUFDLENBQUMsQ0FBQSxDQUFFLFFBQVE7SUFDckcsQ0FBQztJQUVEOzs7T0FHRztJQUNJLG9DQUFrQixHQUF6QixVQUEwQixDQUFPO1FBQy9CLElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDeEUsS0FBZ0IsVUFBa0IsRUFBbEIseUNBQWtCLEVBQWxCLGdDQUFrQixFQUFsQixJQUFrQixFQUFFO1lBQS9CLElBQU0sQ0FBQywyQkFBQTtZQUNWLGFBQWE7WUFDYixJQUFJLFlBQVksR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7WUFDeEcsSUFBSSxZQUFZLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxZQUFZLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTtnQkFDeEQseUNBQXlDO2dCQUN6QyxZQUFZLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMzRCwrREFBK0Q7Z0JBQy9ELElBQUksY0FBYyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUNuRSxnRUFBZ0U7Z0JBQ2hFLElBQU0sU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO2dCQUNqQyxJQUFNLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtnQkFDakMsSUFBSSxNQUFNLEdBQUcsWUFBWSxDQUFDLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFBO2dCQUNoRCxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUE7Z0JBRWxCLHNJQUFzSTtnQkFDdEksSUFBSSxjQUFjLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7b0JBQ2hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFFakIsNEJBQTRCO2dCQUM1QixJQUFNLEdBQUcsR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUV0QywyQ0FBMkM7Z0JBQzNDLElBQUksZUFBZSxHQUFHLElBQUksTUFBTSxFQUFFLENBQUE7Z0JBQ2xDLGVBQWUsQ0FBQyxDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUE7Z0JBQ3pELGVBQWUsQ0FBQyxDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUE7Z0JBRXpELDZCQUE2QjtnQkFDN0IsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNuQixJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO29CQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUM5RCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO29CQUMvRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO2lCQUNyRTtnQkFFRCxPQUFPLGVBQWUsQ0FBQTthQUN2QjtTQUNGO1FBRUQsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBMUVhLDRCQUFVLEdBQVksS0FBSyxDQUFDO0lBQzVCLGlDQUFlLEdBQVksS0FBSyxDQUFDO0lBQ2hDLGtDQUFnQixHQUFXLEVBQUUsQ0FBQTtJQUM3Qiw0QkFBVSxHQUFXLEVBQUUsQ0FBQTtJQXdFeEMsd0JBQUM7Q0FBQSxBQTVFRCxJQTRFQztBQ2pGRCx5Q0FBeUM7QUFDekMsSUFBSSxnQkFBaUMsQ0FBQTtBQUNyQyxJQUFJLE1BQWMsQ0FBQTtBQUNsQixJQUFJLE9BQW9CLENBQUE7QUFDeEIsSUFBSSxNQUFNLEdBQVksS0FBSyxDQUFBO0FBQzNCLElBQUksZUFBMEIsQ0FBQztBQUUvQixTQUFTLEtBQUs7SUFDWixXQUFXLEVBQUUsQ0FBQTtJQUViLE1BQU0sR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFBO0lBRXJCLGlDQUFpQztJQUNqQyxnQkFBZ0IsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUMsUUFBUSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUE7SUFDM0UsZ0JBQWdCLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxDQUFBO0lBRWxDLGtEQUFrRDtJQUNsRCxlQUFlLEdBQUcsSUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0lBQy9DLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFBO0lBRTFELGVBQWU7SUFDZixPQUFPLEdBQUcsRUFBRSxDQUFBO0lBQ1osS0FBZ0IsVUFBK0UsRUFBL0UsTUFBQyxJQUFJLEtBQUssRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLEVBQUUsSUFBSSxHQUFHLEVBQUUsRUFBRSxJQUFJLElBQUksRUFBRSxFQUFFLElBQUksT0FBTyxFQUFFLEVBQUUsSUFBSSxRQUFRLEVBQUUsQ0FBQyxFQUEvRSxjQUErRSxFQUEvRSxJQUErRSxFQUFFO1FBQTVGLElBQU0sQ0FBQyxTQUFBO1FBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNmLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtLQUNsQztBQUNILENBQUM7QUFFRCxTQUFTLElBQUk7SUFDWCxlQUFlO0lBQ2YsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ2YsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFBO0lBQ25DLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTtJQUNuQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUE7SUFDeEIsS0FBZ0IsVUFBTyxFQUFQLG1CQUFPLEVBQVAscUJBQU8sRUFBUCxJQUFPO1FBQWxCLElBQU0sQ0FBQyxnQkFBQTtRQUNWLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtLQUFBO0lBRVosYUFBYTtJQUNiLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtJQUNmLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtJQUNiLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFBLENBQUUscUNBQXFDO0lBQy9ELEtBQWdCLFVBQU8sRUFBUCxtQkFBTyxFQUFQLHFCQUFPLEVBQVAsSUFBTztRQUFsQixJQUFNLENBQUMsZ0JBQUE7UUFDVixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7S0FBQTtBQUNaLENBQUM7QUFFRDs7R0FFRztBQUNILFNBQVMsYUFBYTtJQUNwQixXQUFXLEVBQUUsQ0FBQTtBQUNmLENBQUM7QUFFRCxTQUFTLFdBQVc7SUFDbEIsWUFBWSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUMsQ0FBQTtJQUN2QyxpQkFBaUIsQ0FBQyx3QkFBd0IsRUFBRSxDQUFBO0FBQzlDLENBQUM7QUFFRCxTQUFTLEtBQUs7SUFDWixNQUFNLEdBQUcsSUFBSSxDQUFBO0lBQ2IsTUFBTSxFQUFFLENBQUE7SUFDUixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBQzVCLENBQUM7QUFFRCxTQUFTLE9BQU87SUFDZCxNQUFNLEdBQUcsS0FBSyxDQUFBO0lBQ2QsSUFBSSxFQUFFLENBQUE7SUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO0FBQzlCLENBQUM7QUFFRCxTQUFTLFlBQVk7SUFDbkIsSUFBSSxNQUFNO1FBQ1IsT0FBTyxFQUFFLENBQUE7O1FBRVQsS0FBSyxFQUFFLENBQUE7QUFDWCxDQUFDO0FBRUQ7O0dBRUc7QUFDSCxTQUFTLFVBQVU7SUFDakIsSUFBSSxPQUFPLEtBQUssTUFBTTtRQUNwQixLQUFLLEVBQUUsQ0FBQTtTQUNKLElBQUksR0FBRyxLQUFLLEdBQUc7UUFDbEIsWUFBWSxFQUFFLENBQUE7QUFDbEIsQ0FBQztBQUVELFNBQVMsWUFBWTtJQUNuQixJQUFJLENBQUMsTUFBTTtRQUNULGdCQUFnQixDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUMvRSxDQUFDO0FDekZEO0lBR0U7UUFGQSxVQUFLLEdBQWlCLEVBQUUsQ0FBQTtRQUd0Qix3Q0FBd0M7UUFDeEMsSUFBTSxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUE7UUFDN0QsSUFBTSxpQkFBaUIsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQTtRQUNwRSxJQUFNLGFBQWEsR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDekQsSUFBTSxjQUFjLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssRUFBRSxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDaEUsSUFBTSxtQkFBbUIsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFBO1FBQzNFLElBQU0sb0JBQW9CLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQTtRQUM1RSxJQUFNLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUE7UUFDeEUsSUFBTSxpQkFBaUIsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFBO1FBRXpFLGVBQWU7UUFDZixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDLENBQUE7UUFDdEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUMsbUJBQW1CLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQTtRQUNuRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLFVBQVUsQ0FBQyxhQUFhLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFBO1FBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDLGdCQUFnQixFQUFFLGlCQUFpQixDQUFDLENBQUMsQ0FBQTtRQUNwRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFBO1FBQ2xFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUE7UUFDckUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUMsb0JBQW9CLEVBQUUsaUJBQWlCLENBQUMsQ0FBQyxDQUFBO0lBQzFFLENBQUM7SUFFRCx1QkFBTSxHQUFOO1FBQ0UsS0FBZ0IsVUFBVSxFQUFWLEtBQUEsSUFBSSxDQUFDLEtBQUssRUFBVixjQUFVLEVBQVYsSUFBVTtZQUFyQixJQUFNLENBQUMsU0FBQTtZQUNWLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUFBO0lBQ2QsQ0FBQztJQUVELHFCQUFJLEdBQUo7UUFDRSxLQUFnQixVQUFVLEVBQVYsS0FBQSxJQUFJLENBQUMsS0FBSyxFQUFWLGNBQVUsRUFBVixJQUFVO1lBQXJCLElBQU0sQ0FBQyxTQUFBO1lBQ1YsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO1NBQUE7SUFDWixDQUFDO0lBQ0gsYUFBQztBQUFELENBQUMsQUFqQ0QsSUFpQ0M7QUNqQ0Q7SUFPRSxvQkFBWSxFQUFhLEVBQUUsRUFBYTtRQU54QyxpQkFBWSxHQUFXLEVBQUUsQ0FBQTtRQUN6Qix5QkFBb0IsR0FBVyxFQUFFLENBQUE7UUFDakMsaUJBQVksR0FBVyxDQUFDLENBQUE7UUFLdEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUE7UUFDWixJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQTtRQUVaLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNoRSxDQUFDO0lBRUQsMkJBQU0sR0FBTjtJQUNBLENBQUM7SUFFRCx5QkFBSSxHQUFKO1FBQ0UsSUFBSSxFQUFFLENBQUE7UUFDTixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDVCxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDZixNQUFNLEVBQUUsQ0FBQTtRQUNSLHlIQUF5SDtRQUN6SCxJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFBO1FBQ25ELElBQU0sYUFBYSxHQUFHLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBQ3pELElBQU0sb0JBQW9CLEdBQUcsUUFBUSxHQUFHLGFBQWEsQ0FBQTtRQUVyRCx3R0FBd0c7UUFDeEcsSUFBSSxjQUFjLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQTtRQUU3RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxVQUFVLEVBQUUsQ0FBQTtZQUNaLGlHQUFpRztZQUNqRyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFBO2dCQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLENBQUE7Z0JBQ2xFLElBQUksTUFBTSxHQUFHLFlBQVksRUFBRSxDQUFBO2dCQUMzQixNQUFNLENBQUMsQ0FBQyxHQUFJLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLEVBQUUsVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQTtnQkFDM0YsTUFBTSxDQUFDLENBQUMsR0FBSSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLEdBQUcsR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFBO2dCQUNqRyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUE7Z0JBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtnQkFDOUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtnQkFDbkIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQzdCO1lBQ0QsUUFBUSxFQUFFLENBQUE7U0FDWDtRQUNELEdBQUcsRUFBRSxDQUFBO0lBQ1AsQ0FBQztJQUNILGlCQUFDO0FBQUQsQ0FBQyxBQWhERCxJQWdEQzs7Ozs7Ozs7Ozs7Ozs7QUNoREQ7SUFBb0IseUJBQVM7SUFNM0I7UUFBQSxZQUNFLGlCQUFPLFNBb0JSO1FBeEJELDhCQUF3QixHQUFzQixFQUFFLENBQUE7UUFDaEQsZ0JBQVUsR0FBZ0IsRUFBRSxDQUFBO1FBSzFCLHVFQUF1RTtRQUN2RSxLQUFJLENBQUMsMkJBQTJCLEdBQUcsZUFBZSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixFQUFFLENBQUMsRUFBRSxDQUFDLENBQUE7UUFDcEcsS0FBSSxDQUFDLDJCQUEyQixDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFFLDZDQUE2QztRQUVqRyx3RUFBd0U7UUFDeEUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNoRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7WUFDakUsSUFBTSxhQUFhLEdBQUcsSUFBSSxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDbkUsYUFBYSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7WUFDM0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDbkMsS0FBSSxDQUFDLDJCQUEyQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUU3RCxJQUFJLG9CQUFvQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQywwQkFBd0IsQ0FBRyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUN6RyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBO1lBQy9CLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUNqRCxvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFJLENBQUMsQ0FBQTtZQUNuRCxLQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUE7U0FDekQ7O0lBQ0gsQ0FBQztJQUVELHNCQUFNLEdBQU47UUFDRSx1Q0FBdUM7UUFDdkMsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFFZixLQUFnQixVQUFlLEVBQWYsS0FBQSxJQUFJLENBQUMsVUFBVSxFQUFmLGNBQWUsRUFBZixJQUFlLEVBQUU7WUFBNUIsSUFBTSxDQUFDLFNBQUE7WUFDVixDQUFDLENBQUMsNEJBQTRCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1lBQzFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNYO1FBRUQscURBQXFEO1FBQ3JELElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ3pDLElBQUksSUFBSSxHQUFHLEdBQUc7WUFDWixJQUFJLENBQUMsMkJBQTJCLENBQUMsUUFBUSxHQUFHLENBQUMsRUFBRSxDQUFBOztZQUUvQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFFdEUsQ0FBQztJQUVELG9CQUFJLEdBQUo7UUFDRSxJQUFJLEVBQUUsQ0FBQTtRQUNOLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNULFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNmLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUNSLFVBQVUsRUFBRSxDQUFBO1FBQ1osS0FBZ0IsVUFBZSxFQUFmLEtBQUEsSUFBSSxDQUFDLFVBQVUsRUFBZixjQUFlLEVBQWYsSUFBZTtZQUExQixJQUFNLENBQUMsU0FBQTtZQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQUE7UUFDcEMsUUFBUSxFQUFFLENBQUE7UUFDVixHQUFHLEVBQUUsQ0FBQTtJQUNQLENBQUM7SUF4RE0sdUJBQWlCLEdBQUcsQ0FBQyxDQUFBO0lBeUQ5QixZQUFDO0NBQUEsQUExREQsQ0FBb0IsU0FBUyxHQTBENUI7QUMxREQ7SUFBbUIsd0JBQVM7SUFJMUI7UUFBQSxZQUNFLGlCQUFPLFNBR1I7UUFQTyxpQkFBVyxHQUFXLEVBQUUsQ0FBQTtRQUN4QiwwQkFBb0IsR0FBVyxFQUFFLENBQUE7UUFLdkMsS0FBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUE7O0lBQ3hCLENBQUM7SUFFRCwwQkFBVyxHQUFYLFVBQVksS0FBZ0I7UUFDMUIsaUJBQU0sV0FBVyxZQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsbUJBQUksR0FBSjtRQUNFLElBQUksRUFBRSxDQUFBO1FBQ04sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ1QsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2YsTUFBTSxFQUFFLENBQUE7UUFDUixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFBO1FBQzlDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLElBQUksTUFBTSxHQUFHLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7WUFFL0Isb0NBQW9DO1lBQ3BDLElBQUksaUJBQWlCLEdBQUcsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQTtZQUM1RCxNQUFNLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7WUFFaEMsZUFBZTtZQUNmLElBQUksUUFBTSxHQUFHLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUE7WUFDeEUsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFNLENBQUMsQ0FBQTtZQUVuQixJQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7WUFDN0MsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQTtTQUMvQjtRQUNELEdBQUcsRUFBRSxDQUFBO0lBQ1AsQ0FBQztJQUNILFdBQUM7QUFBRCxDQUFDLEFBcENELENBQW1CLFNBQVMsR0FvQzNCO0FDcENEO0lBQWtCLHVCQUFTO0lBTXpCO1FBQUEsWUFDRSxpQkFBTyxTQXNCUjtRQTFCRCx1QkFBaUIsR0FBc0IsRUFBRSxDQUFBO1FBQ3pDLGdCQUFVLEdBQWdCLEVBQUUsQ0FBQTtRQUsxQixLQUFJLENBQUMsMkJBQTJCLEdBQUcsZUFBZSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQ2xHLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFBO1FBRW5ELHVFQUF1RTtRQUN2RSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzlDLHVCQUF1QjtZQUN2QixJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQzFFLElBQU0sYUFBYSxHQUFHLElBQUksU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ25FLGFBQWEsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1lBQzNCLGFBQWEsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1lBQzNCLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBQ25DLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7WUFFN0QsK0RBQStEO1lBQy9ELElBQUksb0JBQW9CLEdBQUcsZUFBZSxDQUFDLHdCQUF3QixDQUFDLG1CQUFpQixDQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO1lBQ2xHLG9CQUFvQixDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFFLHVCQUF1QjtZQUMvRCxvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFJLENBQUMsQ0FBQTtZQUNuRCxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDakQsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1NBQ2xEOztJQUNILENBQUM7SUFFRCxvQkFBTSxHQUFOO1FBQ0UsdUNBQXVDO1FBQ3ZDLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBRWYsS0FBZ0IsVUFBZSxFQUFmLEtBQUEsSUFBSSxDQUFDLFVBQVUsRUFBZixjQUFlLEVBQWYsSUFBZSxFQUFFO1lBQTVCLElBQU0sQ0FBQyxTQUFBO1lBQ1YsQ0FBQyxDQUFDLDRCQUE0QixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUMxQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUE7U0FDWDtJQUNILENBQUM7SUFFRCxrQkFBSSxHQUFKO1FBQ0UsSUFBSSxFQUFFLENBQUE7UUFDTixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDVCxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDZixLQUFnQixVQUFlLEVBQWYsS0FBQSxJQUFJLENBQUMsVUFBVSxFQUFmLGNBQWUsRUFBZixJQUFlO1lBQTFCLElBQU0sQ0FBQyxTQUFBO1lBQ1YsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FBQTtRQUNuQyxHQUFHLEVBQUUsQ0FBQTtJQUNQLENBQUM7SUEvQ00scUJBQWlCLEdBQVcsQ0FBQyxDQUFBO0lBZ0R0QyxVQUFDO0NBQUEsQUFqREQsQ0FBa0IsU0FBUyxHQWlEMUI7QUNqREQ7SUFBbUIsd0JBQVM7SUFZMUI7UUFBQSxZQUNFLGlCQUFPLFNBeUJSO1FBbkNELG1DQUE2QixHQUFzQixFQUFFLENBQUE7UUFDckQsZ0JBQVUsR0FBZ0IsRUFBRSxDQUFBO1FBQzVCLHlCQUFtQixHQUFXLENBQUMsQ0FBQSxDQUFFLHNDQUFzQztRQUN2RSx3QkFBa0IsR0FBVyxFQUFFLENBQUE7UUFDL0Isd0JBQWtCLEdBQVcsR0FBRyxDQUFBO1FBRWhDLDJCQUFxQixHQUFXLEdBQUcsQ0FBQSxDQUFFLHdDQUF3QztRQUM3RSx5Q0FBbUMsR0FBVyxHQUFHLENBQUE7UUFLL0MsOERBQThEO1FBQzlELEtBQUksQ0FBQywyQkFBMkIsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUVuRyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLHVCQUF1QjtZQUN2QixJQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1lBQ3pDLElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUN0RCxJQUFNLGFBQWEsR0FBRyxJQUFJLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNuRSxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUVuQyx1REFBdUQ7WUFDdkQsS0FBSSxDQUFDLDJCQUEyQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUU3RCwrQkFBK0I7WUFDL0IsSUFBSSxvQkFBb0IsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUMsb0JBQWtCLENBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUE7WUFDcEcsb0JBQW9CLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtZQUNoQyxvQkFBb0IsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBRSx1QkFBdUI7WUFDL0Qsb0JBQW9CLENBQUMsd0JBQXdCLENBQUMsS0FBSSxDQUFDLENBQUE7WUFDbkQsb0JBQW9CLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBQ2pELEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtTQUM5RDtRQUVELEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUE7O0lBQ3BELENBQUM7SUFFRCwwQkFBVyxHQUFYLFVBQVksS0FBZ0I7UUFDMUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsVUFBVTtZQUN2QyxpQkFBTSxXQUFXLFlBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBOztZQUVsQyxpQkFBTSxXQUFXLFlBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELHFCQUFNLEdBQU47UUFDRSx1Q0FBdUM7UUFDdkMsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFFZixpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksVUFBVTtZQUN4QyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFHO2dCQUNqRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUE7Z0JBQ3ZHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUE7YUFDbkQ7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQTthQUNsRTtRQUVILGlFQUFpRTtRQUNqRSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUU1QixDQUFDLENBQUMsNEJBQTRCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1lBRTFDLGlHQUFpRztZQUNqRyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUE7WUFFVix5R0FBeUc7WUFDekcsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQTtZQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDWixNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBOztnQkFFdEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1lBRXpCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDakIsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN0QjtJQUNILENBQUM7SUFFRCxtQkFBSSxHQUFKO1FBQ0UsSUFBSSxFQUFFLENBQUE7UUFDTixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDVCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUU1QixhQUFhO1lBQ2IsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2YsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFakMsd0NBQXdDO1lBQ3hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25ELElBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQzdCLElBQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBRSxnQ0FBZ0M7Z0JBRXRGLGdHQUFnRztnQkFDaEcsSUFBSSxRQUFRLENBQUMsS0FBSyxFQUFFLEdBQUcsR0FBRyxFQUFFO29CQUMxQixZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBQ2YsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQy9EO2FBQ0Y7U0FDRjtRQUNELEdBQUcsRUFBRSxDQUFBO0lBQ1AsQ0FBQztJQXZHTSxzQkFBaUIsR0FBVyxFQUFFLENBQUE7SUF3R3ZDLFdBQUM7Q0FBQSxBQXpHRCxDQUFtQixTQUFTLEdBeUczQjtBQ3pHRDtJQUFzQiwyQkFBUztJQUk3QjtRQUFBLFlBQ0UsaUJBQU8sU0FHUjtRQVBPLGlCQUFXLEdBQVcsRUFBRSxDQUFBO1FBQ3hCLDBCQUFvQixHQUFXLEVBQUUsQ0FBQTtRQUt2QyxLQUFJLENBQUMsU0FBUyxJQUFJLEdBQUcsQ0FBQTs7SUFDdkIsQ0FBQztJQUVELDZCQUFXLEdBQVgsVUFBWSxLQUFnQjtRQUMxQixpQkFBTSxXQUFXLFlBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxzQkFBSSxHQUFKO1FBQ0UsSUFBSSxFQUFFLENBQUE7UUFDTixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDVCxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLENBQUMsQ0FBQyxDQUFBO1FBQzNDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLElBQUksTUFBTSxHQUFHLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEMsa0RBQWtEO1lBQ2xELE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO1lBQ3BELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1lBRXpFLDBEQUEwRDtZQUMxRCxNQUFNLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQTtZQUVmLElBQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUM3QyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDcEI7UUFDRCxHQUFHLEVBQUUsQ0FBQTtJQUNQLENBQUM7SUFDSCxjQUFDO0FBQUQsQ0FBQyxBQWhDRCxDQUFzQixTQUFTLEdBZ0M5QjtBQ2hDRDtJQUF1Qiw0QkFBUztJQWU5QjtRQUFBLFlBQ0UsaUJBQU8sU0FHUjtRQWxCRCxrQkFBWSxHQUFXLENBQUMsQ0FBQTtRQUN4QixxQkFBZSxHQUFXLENBQUMsQ0FBQTtRQUMzQixxQkFBZSxHQUFXLENBQUMsQ0FBQTtRQUMzQixtQ0FBNkIsR0FBc0IsRUFBRSxDQUFBO1FBQ3JELFlBQU0sR0FBa0IsRUFBRSxDQUFBO1FBQzFCLDZCQUF1QixHQUFzQixFQUFFLENBQUE7UUFDL0Msc0NBQXNDO1FBQ3RDLHNCQUFnQixHQUFXLEdBQUcsQ0FBQTtRQUM5QixzQkFBZ0IsR0FBVyxHQUFHLENBQUE7UUFDOUIsMEJBQW9CLEdBQVcsRUFBRSxDQUFBO1FBQ2pDLDBCQUFvQixHQUFXLEdBQUcsQ0FBQTtRQUMxQix1QkFBaUIsR0FBVyxDQUFDLENBQUE7UUFDN0IsZ0JBQVUsR0FBWSxLQUFLLENBQUE7UUFLakMsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFBOztJQUNmLENBQUM7SUFFTyx5QkFBTSxHQUFkO1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFBO1FBRWpHLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFbkMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO0lBQ3hCLENBQUM7SUFFTyxrQ0FBZSxHQUF2QjtRQUNFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFBO1lBRW5CLDBDQUEwQztZQUMxQyxJQUFNLHNCQUFzQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBa0IsQ0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUE7WUFDbkcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO1lBRXpELG9EQUFvRDtZQUNwRCxJQUFNLFlBQVksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUE7WUFDOUUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDckMsK0RBQStEO2dCQUMvRCxJQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUE7Z0JBQ2hDLElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtnQkFDdEQsSUFBTSxhQUFhLEdBQUcsSUFBSSxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ25FLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO2dCQUNsQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7Z0JBRW5ELHdEQUF3RDtnQkFDeEQsSUFBTSxvQkFBb0IsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUMsb0JBQWtCLENBQUMsU0FBSSxDQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFBO2dCQUM1RyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7Z0JBQ2pELG9CQUFvQixDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUNuRCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUE7YUFDOUQ7U0FDRjtJQUNILENBQUM7SUFFTyw0QkFBUyxHQUFqQjtRQUNFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQTtRQUV6RyxLQUFnQixVQUFrQyxFQUFsQyxLQUFBLElBQUksQ0FBQyw2QkFBNkIsRUFBbEMsY0FBa0MsRUFBbEMsSUFBa0M7WUFBN0MsSUFBTSxDQUFDLFNBQUE7WUFDVixDQUFDLENBQUMsT0FBTyxFQUFFLENBQUE7U0FBQTtRQUViLEtBQWdCLFVBQTRCLEVBQTVCLEtBQUEsSUFBSSxDQUFDLHVCQUF1QixFQUE1QixjQUE0QixFQUE1QixJQUE0QjtZQUF2QyxJQUFNLENBQUMsU0FBQTtZQUNWLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtTQUFBO1FBRWIsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFFaEIsaUVBQWlFO1FBQ2pFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFBO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFBO0lBQ3pCLENBQUM7SUFFRCx5QkFBTSxHQUFOO1FBQ0UsdUNBQXVDO1FBQ3ZDLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBRWYsaUNBQWlDO1FBQ2pDLElBQUksVUFBVSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQ3hCLGdDQUFnQztnQkFDaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7b0JBQ2hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFBO2lCQUN4QjtxQkFBTTtvQkFDTCw2Q0FBNkM7b0JBQzdDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFBO29CQUN0QixJQUFJLENBQUMsaUJBQWlCLElBQUksRUFBRSxDQUFBO2lCQUM3Qjs7Z0JBR0QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO1NBQ2hCO1FBRUQsS0FBZ0IsVUFBVyxFQUFYLEtBQUEsSUFBSSxDQUFDLE1BQU0sRUFBWCxjQUFXLEVBQVgsSUFBVyxFQUFFO1lBQXhCLElBQU0sQ0FBQyxTQUFBO1lBQ1YsS0FBa0IsVUFBQyxFQUFELE9BQUMsRUFBRCxlQUFDLEVBQUQsSUFBQyxFQUFFO2dCQUFoQixJQUFNLEdBQUcsVUFBQTtnQkFDWixJQUFJLElBQUksQ0FBQyxVQUFVO29CQUNqQixHQUFHLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBRWxFLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtnQkFDWixHQUFHLENBQUMsNEJBQTRCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO2FBQzdDO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsdUJBQUksR0FBSjtRQUNFLElBQUksRUFBRSxDQUFBO1FBQ04sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBRVQseUNBQXlDO1FBQ3pDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNmLE1BQU0sRUFBRSxDQUFBO1FBQ1IsS0FBZ0IsVUFBVyxFQUFYLEtBQUEsSUFBSSxDQUFDLE1BQU0sRUFBWCxjQUFXLEVBQVgsSUFBVyxFQUFFO1lBQXhCLElBQU0sQ0FBQyxTQUFBO1lBQ1YsVUFBVSxFQUFFLENBQUE7WUFDWixLQUFrQixVQUFDLEVBQUQsT0FBQyxFQUFELGVBQUMsRUFBRCxJQUFDO2dCQUFkLElBQU0sR0FBRyxVQUFBO2dCQUNaLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQUE7WUFDeEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ2hCO1FBQ0QsR0FBRyxFQUFFLENBQUE7SUFDUCxDQUFDO0lBQ0gsZUFBQztBQUFELENBQUMsQUF6SEQsQ0FBdUIsU0FBUyxHQXlIL0IifQ==